//============================================================================
// Name        : WorldWideNet.cpp
// Author      : Steffen Kremer
// Version     :
// Copyright   : 
// Description :
//============================================================================

#include <iostream>
#include "Engine.h"


using namespace std;

#include "SDL.h"
#undef main

int main()
{
	if (!gameEngine.init())
	{
		std::cout << "\n###############\nError: " << gameEngine._error << "\n\n" << gameEngine._errorS << endl << gameEngine._sdlError << "\n###############" << std::endl;
		return 1;
	}
	while(!gameEngine._shutdown)
	{
		gameEngine.update();
		if (gameEngine._error!=0)
		{
			std::cout << "\n###############\nError: " << gameEngine._error << "\n\n" << gameEngine._errorS << endl << gameEngine._sdlError << "\n###############" << std::endl;
			gameEngine._error  = 0;
			gameEngine._errorS  = "";
			//gameEngine._sdlError  = 0;
		}
	}
	gameEngine.quit();
	return 0;
}

/*
 * ResourceManager.h
 *
 *  Created on: Feb 1, 2016
 *      Author: Steffen Kremer
 */

#ifndef INCLUDE_RESOURCEMANAGER_H_
#define INCLUDE_RESOURCEMANAGER_H_

#include <iostream>
#include <vector>
#include "SDL.h"
#include <SDL_mixer.h>
#undef main

class ResourceSound
{
public:
	Mix_Chunk *_sound;
	std::string _soundFile = "";

	bool loadFromFile(std::string);

	void play();
	//void pause();
	//void stop();
	//void setVolume(int volume);

	void destroy();
};

class ResourceMusic
{
public:
	Mix_Music *_music;
	std::string _musicFile = "";

	bool loadFromFile(std::string);

	void play();
	//void pause();
	//void stop();
	void setVolume(int volume);

	void destroy();
};

class ResourceFont
{
public:
	TTF_Font* _font = NULL;//static_cast<TTF_Font*>0xdeadbeef;
	int _fontSize = 3;
	std::string _fontFile = "";

	bool loadFromFile(std::string file, int fSize);

	void destroy();
};

class ResourceTexture
{
public:
	SDL_Texture *_texture = NULL;

	SDL_Rect _size;
	SDL_Rect _position;

	std::string _data = ""; //File/Text
	bool _visible = true;

	bool _isText = false;
	int _fSize = 3;
	SDL_Color _textColor = {70,30,120};

	bool createFromFile(std::string file, int x, int y, float sx, float sy);
	bool createFromText(std::string text, int fSize, int x, int y, SDL_Color foreGround);
	bool changeText(std::string text);

	void resize(float sx, float sy);

	void update(int x, int y);
	void draw();
	void destroy();
};

class ResourceManager
{
public:
	std::vector< ResourceFont* > _resourceFonts;
	std::vector< ResourceTexture* > _resourceTextures;
	//std::vector< ResourceSound* > _resourceSounds;

	ResourceMusic _gameMusic;
	ResourceSound _gameSoundGUIButtonPressed;
<<<<<<< HEAD
	ResourceSound _gameSoundGameStartup;
	ResourceSound _gameSoundGameShutdown;

	TTF_Font* addFont(std::string fontFile, int fSize);

	void destroy();
=======

	TTF_Font* addFont(std::string fontFile, int fSize);
>>>>>>> 59b8ca550010837924817d1561f03a061633f6b2
};

extern ResourceManager engineResource;

#endif /* INCLUDE_RESOURCEMANAGER_H_ */

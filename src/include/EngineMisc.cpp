/*
 * EngineMisc.cpp
 *
 *  Created on: Jan 29, 2016
 *      Author: Steffen Kremer
 */

#include "EngineMisc.h"

#if defined(__MINGW32__) || defined(__MINGW64__)
#include <windows.h>
#else
#include <unistd.h>
#endif

void mSleep(int milliS)
{
	#if defined(__MINGW32__) || defined(__MINGW64__)
	::Sleep((DWORD)milliS);
	#else
	sleep(milliS);
	#endif
}

int randomRange(int min, int max)
{
	return min + (rand() % (int)(max - min + 1));
}

bool posIsInRect(int x, int y, SDL_Rect rect)
{
	if (x>rect.x&&x<rect.x+rect.w)
	{
		if (y>rect.y&&y<rect.y+rect.h)
		{
			return true;
		}
	}
	return false;
}

bool posIsInRect(int x, int y, int rx, int ry, int rw, int rh)
{
	if (x>rx&&x<rx+rw)
	{
		if (y>ry&&y<ry+rh)
		{
			return true;
		}
	}
	return false;
}

unsigned int charToUInt32(unsigned char *a)
{
	return *(int *)a;
}

int charToInt32(unsigned char *a)
{
	return *(int *)a;
}

unsigned int charToUInt16(unsigned char *a)
{
	//unsigned char *b = a+1;
	return (*(a+1) << 8) | *a;
}

int charToInt16(unsigned char *a)
{
	//unsigned char *b = a+1;
	return (*(a+1) << 8) | *a;
}

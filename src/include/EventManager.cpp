/*
 * EventManager.cpp
 *
 *  Created on: Jan 29, 2016
 *      Author: Steffen Kremer
 */

#include <iostream>
#include <vector>
#include "EventManager.h"
#include "GUI.h"

bool EventManager::init()
{
	return true;
}

void EventManager::update()
{

}

void EventManager::pushEvent(EVENT_TYPE event, EVENT_LOCATION loc)
{
	if (loc==EVENT_LOCATION_GUI)
	{
		engineGUI._guiEvents.insert(engineGUI._guiEvents.end(), new Event());
		engineGUI._guiEvents.at(engineGUI._guiEvents.size()-1)->_eventType = event;
	}
	else if (loc==EVENT_LOCATION_ENGINE)
	{
		this->_engineEvents.insert(this->_engineEvents.end(), new Event());
		this->_engineEvents.at(this->_engineEvents.size()-1)->_eventType = event;
	}
	else if (loc==EVENT_LOCATION_NETWORK)
	{
		//Zugriff zu NetworkManager
	}
}

EVENT_TYPE EventManager::popEvent(EVENT_LOCATION loc)
{
	EVENT_TYPE t = EVENT_NONE;
	if (loc==EVENT_LOCATION_GUI)
	{
		if (engineGUI._guiEvents.size()==0)
		{
			t = EVENT_NONE;
		}
		else
		{
			t = engineGUI._guiEvents.at(engineGUI._guiEvents.size()-1)->_eventType;
			engineGUI._guiEvents.erase(engineGUI._guiEvents.end());
		}
	}
	else if (loc==EVENT_LOCATION_ENGINE)
	{
		if (this->_engineEvents.size()==0)
		{
			t = EVENT_NONE;
		}
		else
		{
			t = this->_engineEvents.at(this->_engineEvents.size()-1)->_eventType;
			this->_engineEvents.erase(this->_engineEvents.end());
		}
	}
	else if (loc==EVENT_LOCATION_NETWORK)
	{
		//Zugriff zu Network Manager
	}
	return t;
}

EventManager engineEvent;

/*
 * Engine.cpp
 *
 *  Created on: Jan 17, 2016
 *      Author: Steffen Kremer
 */

#include <functional>
#include <iostream>
#include <string>
#include "Engine.h"
#include "EngineMisc.h"
#include "EngineError.h"
#include "ResourceManager.h"
#include "RoomManager.h"
#include "GUI.h"
#include "SDL.h"
#include "SDL_ttf.h"
#undef main

bool Engine::init()
{
	if (!(this->initGraphic()&&this->initSound()&&this->initEvent()&&this->initGUI()&&this->initNetwork()&&this->initManagers()))
	{
		this->_sdlInited = false;
		return false;
	}
	this->_sdlInited = true;
	this->_engineInited = true;
<<<<<<< HEAD
	engineResource._gameSoundGUIButtonPressed.loadFromFile(".\\data\\sounds\\buttonPressed.wav");
	engineResource._gameSoundGameStartup.loadFromFile(".\\data\\sounds\\startup.wav");
	engineResource._gameSoundGameStartup.play();
	engineResource._gameSoundGameShutdown.loadFromFile(".\\data\\sounds\\shutdown.wav");

	SDL_Delay(2000);
	gameRoom.loadFromFile(".\\data\\rooms\\wwn_intro.dat");
	if (gameRoom._success)
	{
		engineGUI.showMouseCursor(false);

		gameEngine.update();
		SDL_Delay(1500);
		engineGUI._guiWindows.at(0)->createGadgetLable("Booting from A: ...",10,10,100,30);
		gameEngine.update();
		SDL_Delay(1500);
		engineGUI._guiWindows.at(0)->createGadgetLable("Stopping POST...",10,40,100,30);
		gameEngine.update();
		SDL_Delay(1500);
		engineGUI._guiWindows.at(0)->createGadgetLable("Interrupt IRQ lines...",10,70,100,30);
		gameEngine.update();
		SDL_Delay(1500);
		engineGUI._guiWindows.at(0)->createGadgetLable("Loading wnnOS...",10,100,100,30);
		gameEngine.update();
		SDL_Delay(1000);
	}
	else
	{
		SDL_Delay(8000);
	}
	engineResource._gameMusic.loadFromFile("..\\..\\Matrix_Theme_Song.wav");
	engineResource._gameMusic.setVolume(20);
	//engineResource._gameMusic.play();

	gameRoom.loadFromFile(".\\data\\rooms\\wwn_menu.dat");
	engineGUI.showMouseCursor(true);
=======
	engineResource._gameMusic.loadFromFile("..\\..\\Matrix_Theme_Song.wav");
	engineResource._gameMusic.setVolume(20);
	engineResource._gameMusic.play();
	engineResource._gameSoundGUIButtonPressed.loadFromFile("..\\..\\buttonPressed.wav");

	gameRoom.loadFromFile(".\\data\\rooms\\wwn_menu.dat");

	//gameRoom.loadFromFolder(".\\data\\rooms");
>>>>>>> 59b8ca550010837924817d1561f03a061633f6b2

	return true;
}

bool Engine::initGraphic()
{
	if (SDL_Init(SDL_INIT_VIDEO) != 0)
	{
		this->error(1,"SDL_INIT_VIDEO");
		return false;
	}
	unsigned int flags = 0;
	if (this->_displayFullscreen)
	{
		flags = SDL_WINDOW_FULLSCREEN | SDL_WINDOW_SHOWN;
	}
	else
	{
		flags = SDL_WINDOW_SHOWN;
	}
	this->_sdlWindow = SDL_CreateWindow(this->_gameTitle.c_str(), SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, this->_displaySizeX, this->_displaySizeY, flags);
	if (this->_sdlWindow == nullptr){
		this->error(2,"SDL_CreateWindow");
		return false;
	}
	this->_sdlRenderer = SDL_CreateRenderer(this->_sdlWindow, -1, SDL_RENDERER_ACCELERATED);
	if (this->_sdlRenderer == nullptr)
	{
		this->error(3,"SDL_CreateRenderer");
		return false;
	}
	this->_sdlPixelFormat = SDL_GetWindowPixelFormat(this->_sdlWindow);
	this->_sdlSurface = SDL_GetWindowSurface(this->_sdlWindow);
	if (this->_error!=0)
		return false;
    SDL_RenderClear(this->_sdlRenderer);
    SDL_RenderPresent(this->_sdlRenderer);
    this->_fpsTimer.start();
	return true;
}

bool Engine::initSound()
{
	if (SDL_InitSubSystem(SDL_INIT_AUDIO)!=0)
	{
		this->error(4,"SDL_InitAudio");
		return false;
	}
	if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2*2048) < 0)
	{
		this->error(4,"Mix_OpenAudio");
		return false;
	}
	return true;
}

bool Engine::initEvent()
{
	if (SDL_InitSubSystem(SDL_INIT_EVENTS)!=0)
	{
		this->error(5,"SDL_InitEvents");
		return false;
	}
	return true;
}

bool Engine::initNetwork()
{
	//error 6
	return true;
}

bool Engine::initGUI()
{
	if (TTF_Init()==-1)
	{
		this->error(8,"TTF_Init");
		return false;
	}
	SDL_ShowCursor(0);
	if (this->_debugDraw)
	{
		this->_fpsText = new ResourceTexture();
		this->_fpsText->createFromText("X", 18, 10, 10, SDL_Color{255,0,0});
	}
	return true;
}

bool Engine::initManagers()
{
	return engineEvent.init();
}

//########################################################

void Engine::update()
{
	/*if (!this->_engineInited)
	{
		this->error(0,"gameEngine not inited!");
		this->_shutdown = true;
		return;
	}*/
	this->_fpsCapTimer.start();
	this->updateEvent();
	this->updateNetwork();
	this->updateEngine();
	this->updateGUI();
	this->updateSound();
	this->_fpsAverage = this->_fpsCountedFrames / (this->_fpsTimer.getTicks() / 1000.0f);
	if(this->_fpsAverage > 2000000)
	{
		this->_fpsAverage = 0;
	}
	this->updateGraphics();
	this->_fpsCountedFrames++;
	int frameTicks = this->_fpsCapTimer.getTicks();
	if ((unsigned int)frameTicks < this->_fpsScreenTicks)
	{
		SDL_Delay(this->_fpsScreenTicks - frameTicks);
	}
}

void Engine::updateEvent()
{
	while(SDL_PollEvent(&this->_sdlEvent))
	{
		if (this->_sdlEvent.type == SDL_QUIT) {
			this->_shutdown = true;
		}
	}
	engineGUI._mouseStateXmove = engineGUI._mouseStateX;
	engineGUI._mouseStateYmove = engineGUI._mouseStateY;
	unsigned int mouseState = SDL_GetMouseState(&engineGUI._mouseStateX, &engineGUI._mouseStateY);
	engineGUI._mouseStateXmove = -(engineGUI._mouseStateXmove-engineGUI._mouseStateX);
	engineGUI._mouseStateYmove = -(engineGUI._mouseStateYmove-engineGUI._mouseStateY);
	engineGUI._mouseStateLeftStateLast = engineGUI._mouseStateLeftState;
	engineGUI._mouseStateRightStateLast = engineGUI._mouseStateRightState;
	engineGUI._mouseStateLeftState = mouseState & SDL_BUTTON(SDL_BUTTON_LEFT);
	engineGUI._mouseStateRightState = mouseState & SDL_BUTTON(SDL_BUTTON_RIGHT);
}

void Engine::updateNetwork()
{

}

void Engine::updateEngine()
{

}

void Engine::updateGUI()
{
	engineGUI.update();
	if (this->_debugDraw)
	{
		this->_fpsText->changeText("FPS: "+std::to_string(this->_fpsAverage));
	}
}

void Engine::updateSound()
{

}

void Engine::updateGraphics()
{
	SDL_SetRenderDrawColor(this->_sdlRenderer,0,0,0,128);
	SDL_RenderClear(this->_sdlRenderer);
	//GUI
	engineGUI.draw();
	if (this->_debugDraw)
	{
		this->_fpsText->draw();
	}

	SDL_RenderPresent(this->_sdlRenderer);
}

//########################################################

void Engine::quit()
{
	this->_shutdown = true;
<<<<<<< HEAD
	mSleep(300);
	if (this->_alreadyShutdown)
	{
		return;
	}
	this->_alreadyShutdown = true;
	engineGUI.showMouseCursor(false);
	gameRoom.loadFromFile(".\\data\\rooms\\wwn_intro.dat");
	if (gameRoom._success)
	{
		engineGUI._guiWindows.at(0)->changeTitle("Shutting down...");
		engineResource._gameSoundGameShutdown.play();
		gameEngine.update();
		SDL_Delay(800);
		engineGUI._guiWindows.at(0)->createGadgetLable("Stopping daemons...",10,10,100,30);
		gameEngine.update();
		SDL_Delay(900);
		engineGUI._guiWindows.at(0)->createGadgetLable("Saving files..",10,40,100,30);
		gameEngine.update();
		SDL_Delay(800);
		engineGUI._guiWindows.at(0)->createGadgetLable("Stopping wnnOS...",10,70,100,30);
		gameEngine.update();
		SDL_Delay(800);
		engineGUI._guiWindows.at(0)->createGadgetLable("Halt machine...",10,100,100,30);
		gameEngine.update();
		SDL_Delay(750);
	}
	gameRoom.unloadRoom();
	gameEngine.update();
	SDL_Delay(100);
	engineResource.destroy();
=======
>>>>>>> 59b8ca550010837924817d1561f03a061633f6b2
	Mix_Quit();
	TTF_Quit();
	SDL_Quit();
}

//########################################################

void Engine::error(int errorC, std::string errorS)
{
	this->_error=errorC;
	this->_errorS=errorS+'\n';
}

Engine gameEngine;

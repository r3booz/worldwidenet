/*
 * GUI.h
 *
 *  Created on: Jan 19, 2016
 *      Author: Steffen Kremer
 */

#ifndef INCLUDE_GUI_H_
#define INCLUDE_GUI_H_

#include <iostream>
#include <functional>
#include <vector>
#include "SDL.h"
#include "SDL_ttf.h"
#include "EventManager.h"
#include "ResourceManager.h"
#undef main

class GUIGadgetLable;
class GUIGadgetButton;
class GUIImage;
class GUIText;
class Event;
class Engine;

class GUIWindow
{
public:
	bool _moveAble = true;
	bool _resizeAble = false;
	bool _closeAble = true;
	bool _minimizeAble = true;
	bool _maximizeAble = true;
	bool _borderLess = false;
	bool _popUp = false;

	SDL_Color _backColor;

	std::string _title = "";
	GUIText *_titleGUItext;

	unsigned int _windowID = 4294967295;

	//Obvious States
	bool _visible = true;
	bool _maximized = false;
	bool _maximizedLast = false;
	SDL_Rect _maximizedOldPosition;
	bool _minimized = false;

	//Hidden States (Logic)
	bool _grabbed = false;
	bool _closed = false;
	bool _closedAlready = false;
	bool _focused = true;

	std::vector< Event* > _windowEvents;
	void pushEvent(EVENT_TYPE event);
	EVENT_TYPE popEvent();

	std::vector< GUIGadgetLable* > _guiGadgetLables;
	std::vector< GUIGadgetButton* > _guiGadgetButtons;
	GUIGadgetButton* _guiGadgetButtonLastPressed = 0;
	std::vector< GUIImage* > _guiGadgetImages;

	SDL_Rect _position;

	GUIWindow(std::string title, int x, int y, int sx, int sy);

	bool changeTitle(std::string text);
	void setBackColor(Uint8 r, Uint8 g, Uint8 b);
	void setProperties(bool moveAble, bool resizeAble, bool closeAble, bool minimizeAble, bool maximizeAble, bool borderLess, bool popUp);

	GUIGadgetLable* createGadgetLable(std::string lable, int x, int y, int sx, int sy);
	GUIGadgetButton* createGadgetButton(std::string lable, int x, int y, int sx, int sy);
	GUIImage* createGadgetImage(std::string file, int x, int y, float sx, float sy);
	//void createGadgetImage(std::string file, int x, int y, float sx, float sy);

	unsigned int findButtonById(unsigned int id);
<<<<<<< HEAD
	unsigned int findLableById(unsigned int id);
=======
>>>>>>> 59b8ca550010837924817d1561f03a061633f6b2

	void draw();
	void update();
	void updateBackward();
	void updateGadgets();
	void destroy();
};

class GUIImage
{
public:
	GUIWindow *_parentWindow;

	ResourceTexture *_resourceTexture;

    void setVisible(bool visible);

    GUIImage(GUIWindow *pWindow, int x, int y, float sx, float sy, std::string file);
    void draw();
    void update();
    void destroy();
};

class GUIText
{
public:
	ResourceTexture *_resourceTexture;

	void setVisible(bool visible);
	bool changeText(std::string text);

	GUIText(std::string text, int x, int y, int fSize, Uint8 r, Uint8 g, Uint8 b);
	void update(int x, int y);
	void draw();
	void destroy();
};

enum TEXT_ALIGN_HOR {LEFT, CENTER, RIGHT};
enum TEXT_ALIGN_VER {TOP, MIDDLE, BOTTOM};

class GUIGadgetLable
{
public:
	GUIWindow *_parentWindow;

	SDL_Rect _rect;
	SDL_Rect _size;

	GUIText *_guiText;
	unsigned int _lableID = 4294967295;
	TEXT_ALIGN_HOR _horizontalAlign = LEFT;
	TEXT_ALIGN_VER _verticalAlign = MIDDLE;

	GUIGadgetLable(GUIWindow *pWindow, std::string lable, int x, int y, int sx, int sy);
	void setVisible(bool visible);
	bool changeText(std::string text);
	void setHorizontalAlign(TEXT_ALIGN_HOR newAlign);
	void setVerticalAlign(TEXT_ALIGN_VER newAlign);

	void draw();
	void update();
	void destroy();
};

typedef void (Engine::*engineCallbackPointer)();

class GUIGadgetButton
{
public:
	GUIWindow *_parentWindow;

	SDL_Rect _rect;
	SDL_Rect _size;

	GUIGadgetLable *_lableText;

	std::string _lable = "";
	bool _toggleAble = false;
	unsigned int _buttonID = 4294967295;

	//engineCallbackPointer _functionCallback = NULL;
	//void (*_functionCallback)() = NULL;
<<<<<<< HEAD
	/*std::function<void(void)> _functionCallbackVoid;
	std::function<void(std::string)> _functionCallbackString;*/
	//std::vector<std::string> _parameterCallback;
	//std::string _parameterCallback = "";

	std::string _callBack = "";
	std::string _callBackParameter = "";
=======
	std::function<void(void)> _functionCallback;
>>>>>>> 59b8ca550010837924817d1561f03a061633f6b2

	bool _pressed = false;
	bool _toggled = false;
	bool _hover = false;

	bool _visible = true;

	bool changeText(std::string text);
	void setCallback(std::function<void(void)> callback);

	/*void setCallbackVoid(std::function<void(void)> callback);
	void setCallbackString(std::function<void(std::string)> callback);*/

	GUIGadgetButton(GUIWindow *pWindow, std::string lable, int x, int y, int sx, int sy);
	void draw();
	void update();
	void destroy();
};

class GUI
{
public:
	std::vector< GUIWindow* > _guiWindows;
	std::vector< GUIImage* > _guiImage;

	bool _mouseStateLeftState = false;
	bool _mouseStateLeftStateLast = false;
	bool _mouseStateRightState = false;
	bool _mouseStateRightStateLast = false;
	int _mouseStateX = 0;
	int _mouseStateY = 0;
	int _mouseStateXmove = 0;
	int _mouseStateYmove = 0;

	bool _showMouseCursor = true;

	bool _windowAlreadyCollided = false;

	std::vector< Event* > _guiEvents;

	GUIWindow* createWindow(std::string title, int x, int y, int sx, int sy);
	GUIImage* createImage(std::string file, int x, int y, float sx, float sy);
	bool createTexture(int x, int y, float sx, float sy, std::string file);

<<<<<<< HEAD
	void showMouseCursor(bool show);

=======
>>>>>>> 59b8ca550010837924817d1561f03a061633f6b2
	unsigned int findWindowByPointer(GUIWindow* pointer);
	unsigned int findWindowByID(unsigned int windowID);
	void destroyWindow(unsigned int index);
	void destroyAllWindows();
	void destroyAllImages();

	void draw();
	void update();
};

extern GUI engineGUI;


#endif /* INCLUDE_GUI_H_ */

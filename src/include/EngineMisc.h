/*
 * EngineMisc.h
 *
 *  Created on: Jan 17, 2016
 *      Author: Steffen Kremer
 */

#ifndef INCLUDE_ENGINEMISC_H_
#define INCLUDE_ENGINEMISC_H_

#include "SDL.h"
#undef main

void mSleep(int milliS);
int randomRange(int min, int max);

bool posIsInRect(int x, int y, SDL_Rect rect);
bool posIsInRect(int x, int y, int rx, int ry, int rw, int rh);

unsigned int charToUInt32(unsigned char *a);
unsigned int charToUInt16(unsigned char *a);

int charToInt32(unsigned char *a);
int charToInt16(unsigned char *a);

#endif /* INCLUDE_ENGINEMISC_H_ */

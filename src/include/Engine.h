/*
 * Engine.h
 *
 *  Created on: Jan 17, 2016
 *      Author: Steffen Kremer
 */

#ifndef INCLUDE_ENGINE_H_
#define INCLUDE_ENGINE_H_

#include <iostream>
#include "SDL.h"
#include "SDL_ttf.h"
#undef main
#include "ResourceManager.h"
#include "GUI.h"
#include "LTimer.h"
#include "RoomManager.h"

class Engine
{
public:
	bool _engineInited = false;
	bool _sdlInited = false;
	int _error = 0;
	std::string _errorS = "";
	const char *_sdlError;

	bool _shutdown = false;
	bool _alreadyShutdown = false;

	bool _debugDraw = false;

	SDL_Event _sdlEvent;
	SDL_Window *_sdlWindow = NULL;
	SDL_Renderer *_sdlRenderer = NULL;
	SDL_Surface *_sdlSurface = NULL;
	Uint32 _sdlPixelFormat = 0;

	unsigned int _engineVersion = 0;

	std::string _gameTitle = "WorldWideWeb-ALPHA";
	unsigned int _gameVersion = 0;

	unsigned int _displaySizeX = 1366;
	//unsigned int _displaySizeX = 1920;
	unsigned int _displaySizeY = 768;
	//unsigned int _displaySizeY = 1080;
	bool _displayFullscreen = true;
	float _displayDensityX = 1.0f;
	float _displayDensityY = 1.0f;
	float _fpsAverage = 0.0f;
	unsigned int _fpsTarget = 30;
	unsigned int _fpsCurrectFrame = 0;
	unsigned int _fpsCountedFrames = 0;
	unsigned int _fpsScreenTicks = 1000/_fpsTarget;
	LTimer _fpsTimer;
	LTimer _fpsCapTimer;
	ResourceTexture *_fpsText;

	//Files
	const char *_sdlTTFfontFile = ".\\data\\fonts\\lucida.ttf";

	//Engine();
	bool init();
	void update();
	void quit();
	bool sdlInited() {return this->_sdlInited;}

	void error(int errorC, std::string errorS);

private:
	bool initGraphic();
	bool initSound();
	bool initEvent();
	bool initNetwork();
	bool initGUI();
	bool initManagers();

	void updateEvent();
	void updateGUI();
	void updateEngine();
	void updateSound();
	void updateGraphics();
	void updateNetwork();
};

extern Engine gameEngine;

#endif /* INCLUDE_ENGINE_H_ */

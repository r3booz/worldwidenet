/*
 * RoomManager.cpp
 *
 *  Created on: Feb 4, 2016
 *      Author: Steffen Kremer
 */

#include <iostream>
#include <vector>
#include <fstream>
#include "RoomManager.h"
#include "Engine.h"
#include "EngineMisc.h"

bool Room::runBuffer(std::vector<unsigned char> buffer)
{
	unsigned int currentChar = 0;
	unsigned int opCode = 0;
	while(currentChar<buffer.size())
	{
	opCode = charToUInt16(&buffer[currentChar]);
	currentChar+=2;
<<<<<<< HEAD
=======
	//std::cout << " " << opCode  << std::endl;
>>>>>>> 59b8ca550010837924817d1561f03a061633f6b2
	if (opCode==1)
	{
		unsigned int windowID;
		int x,y,sx,sy;
		std::string imageFile = "";
		windowID = charToUInt32(&buffer[currentChar]);
		currentChar+=4;
		for (unsigned int i=0;currentChar<buffer.size();currentChar++)
		{
			if (i) i=0;
			if (buffer.at(currentChar)!=0)
			{
				imageFile += buffer.at(currentChar);
			}
			else
			{
				currentChar++;
				break;
			}
		}
		x = charToInt32(&buffer[currentChar]);
		currentChar+=4;
		y = charToInt32(&buffer[currentChar]);
		currentChar+=4;
		sx = charToInt32(&buffer[currentChar]);
		currentChar+=4;
		sy = charToInt32(&buffer[currentChar]);
		currentChar+=4;
		if (windowID!=4294967295)
		{
			if (engineGUI.findWindowByID(windowID)!=4294967295)
			{
				engineGUI._guiWindows.at(engineGUI.findWindowByID(windowID))->createGadgetImage(imageFile,x,y,sx,sy);
			}
		}
		else
		{
			engineGUI.createImage(imageFile,x,y,sx,sy);
		}
		currentChar++;
	}
	else if (opCode==2)
	{
		unsigned int windowID;
		int x,y,sx,sy;
		std::string windowTitle = "";
		windowID = charToUInt32(&buffer[currentChar]);
		currentChar+=4;
		for (unsigned int i=0;currentChar<buffer.size();currentChar++)
		{
			if (i) i=0;
			if (buffer.at(currentChar)!=0)
			{
				windowTitle += buffer.at(currentChar);
			}
			else
			{
				currentChar++;
				break;
			}
		}
		x = charToInt32(&buffer[currentChar]);
		currentChar+=4;
		y = charToInt32(&buffer[currentChar]);
		currentChar+=4;
		sx = charToInt32(&buffer[currentChar]);
		currentChar+=4;
		sy = charToInt32(&buffer[currentChar]);
		currentChar+=4;
		if (engineGUI.findWindowByID(windowID)==4294967295)
		{
			engineGUI.createWindow(windowTitle,x,y,sx,sy)->_windowID = windowID;
		}
		currentChar++;
	}
	else if (opCode==3)
	{
		int windowID = charToInt32(&buffer[currentChar]);
		currentChar+=4;
		if (engineGUI.findWindowByID(windowID)!=4294967295)
		{
			bool moveAble = (bool)buffer[currentChar];
			currentChar++;
			bool resizeAble = (bool)buffer[currentChar];
			currentChar++;
			bool closeAble = (bool)buffer[currentChar];
			currentChar++;
			bool minimizeAble = (bool)buffer[currentChar];
			currentChar++;
			bool maximizeAble = (bool)buffer[currentChar];
			currentChar++;
			bool borderLess = (bool)buffer[currentChar];
			currentChar++;
			bool popUp = (bool)buffer[currentChar];
			currentChar++;
			engineGUI._guiWindows.at(engineGUI.findWindowByID(windowID))->setProperties(moveAble,resizeAble,closeAble,minimizeAble,maximizeAble,borderLess,popUp);
		}
		currentChar++;
	}
	else if (opCode==4)
	{
		unsigned int windowID, buttonID;
		int x,y,sx,sy;
		std::string buttonTitle = "";
		windowID = charToUInt32(&buffer[currentChar]);
		currentChar+=4;
		buttonID = charToUInt32(&buffer[currentChar]);
		currentChar+=4;
		for (unsigned int i=0;currentChar<buffer.size();currentChar++)
		{
			if (i) i=0;
			if (buffer.at(currentChar)!=0)
			{
				buttonTitle += buffer.at(currentChar);
			}
			else
			{
				currentChar++;
				break;
			}
		}
		x = charToInt32(&buffer[currentChar]);
		currentChar+=4;
		y = charToInt32(&buffer[currentChar]);
		currentChar+=4;
		sx = charToInt32(&buffer[currentChar]);
		currentChar+=4;
		sy = charToInt32(&buffer[currentChar]);
		currentChar+=4;
		if (engineGUI.findWindowByID(windowID)!=4294967295)
		{
			engineGUI._guiWindows.at(engineGUI.findWindowByID(windowID))->createGadgetButton(buttonTitle,x,y,sx,sy)->_buttonID = buttonID;
		}
		currentChar++;
	}
	else if (opCode==5)
	{
<<<<<<< HEAD
		unsigned int windowID,lableID;
=======
		unsigned int windowID;
>>>>>>> 59b8ca550010837924817d1561f03a061633f6b2
		int x,y,sx,sy;
		std::string lableTitle = "";
		windowID = charToUInt32(&buffer[currentChar]);
		currentChar+=4;
<<<<<<< HEAD
		lableID = charToUInt32(&buffer[currentChar]);
		currentChar+=4;
=======
>>>>>>> 59b8ca550010837924817d1561f03a061633f6b2
		for (unsigned int i=0;currentChar<buffer.size();currentChar++)
		{
			if (i) i=0;
			if (buffer.at(currentChar)!=0)
			{
				lableTitle += buffer.at(currentChar);
			}
			else
			{
				currentChar++;
				break;
			}
		}
		x = charToInt32(&buffer[currentChar]);
		currentChar+=4;
		y = charToInt32(&buffer[currentChar]);
		currentChar+=4;
		sx = charToInt32(&buffer[currentChar]);
		currentChar+=4;
		sy = charToInt32(&buffer[currentChar]);
		currentChar+=4;
		if (engineGUI.findWindowByID(windowID)!=4294967295)
		{
<<<<<<< HEAD
			engineGUI._guiWindows.at(engineGUI.findWindowByID(windowID))->createGadgetLable(lableTitle,x,y,sx,sy)->_lableID = lableID;
=======
			engineGUI._guiWindows.at(engineGUI.findWindowByID(windowID))->createGadgetLable(lableTitle,x,y,sx,sy);
>>>>>>> 59b8ca550010837924817d1561f03a061633f6b2
		}
		currentChar++;
	}
	else if (opCode==6)
	{
		unsigned int windowID, buttonID;
		std::string callBackFunction = "";
		windowID = charToInt32(&buffer[currentChar]);
		currentChar+=4;
		buttonID = charToInt32(&buffer[currentChar]);
		currentChar+=4;
<<<<<<< HEAD
		unsigned char parameterCount = buffer.at(currentChar);
		currentChar++;
		std::vector<std::string> parameter;
=======
>>>>>>> 59b8ca550010837924817d1561f03a061633f6b2
		for (unsigned int i=0;currentChar<buffer.size();currentChar++)
		{
			if (i) i=0;
			if (buffer.at(currentChar)!=0)
			{
				callBackFunction += buffer.at(currentChar);
			}
			else
			{
				currentChar++;
				break;
			}
		}
<<<<<<< HEAD
		if (parameterCount>0)
		{
		for(unsigned char ii=0;ii<parameterCount;ii++)
		{
			std::string paraNew = "";
			for (unsigned int i=0;currentChar<buffer.size();currentChar++)
			{
				if (i) i=0;
				if (buffer.at(currentChar)!=0)
				{
					paraNew += buffer.at(currentChar);
				}
				else
				{
					currentChar++;
					parameter.insert(parameter.end(),paraNew);
					break;
				}
			}

		}
		}
		else
		{
			parameter.insert(parameter.end(),"");
		}
		unsigned int wID = engineGUI.findWindowByID(windowID);
		if (wID!=4294967295)
		{
			unsigned int bID = engineGUI._guiWindows.at(engineGUI.findWindowByID(windowID))->findButtonById(buttonID);
			if (bID!=4294967295)
			{
				engineGUI._guiWindows.at(wID)->_guiGadgetButtons.at(bID)->_callBack = callBackFunction;
				engineGUI._guiWindows.at(wID)->_guiGadgetButtons.at(bID)->_callBackParameter = parameter.at(0) ;
			}
		}
		/*if (callBackFunction=="Engine::quit")
		{
			unsigned int wID = engineGUI.findWindowByID(windowID);
			if (wID!=4294967295)
			{
				unsigned int bID = engineGUI._guiWindows.at(engineGUI.findWindowByID(windowID))->findButtonById(buttonID);
				if (bID!=4294967295)
				{
					engineGUI._guiWindows.at(wID)->_guiGadgetButtons.at(bID)->setCallbackVoid([&](){ gameEngine.quit(); });
				}
			}
		}
		else if (callBackFunction=="RoomManager::loadRoom")
		{
			unsigned int wID = engineGUI.findWindowByID(windowID);
			if (wID!=4294967295)
			{
				unsigned int bID = engineGUI._guiWindows.at(engineGUI.findWindowByID(windowID))->findButtonById(buttonID);
				std::cout << bID;
				if (bID!=4294967295)
				{
					engineGUI._guiWindows.at(wID)->_guiGadgetButtons.at(bID)->_parameterCallback = parameter.at(0);
					engineGUI._guiWindows.at(wID)->_guiGadgetButtons.at(bID)->setCallbackString([&](){ gameRoom.loadFromFile(engineGUI._guiWindows.at(wID)->_guiGadgetButtons.at(bID)->_parameterCallback); });
				}
			}
		}*/
		currentChar++;
	}
	else if (opCode==7)
	{
		unsigned int windowID,lableID;

		windowID = charToInt32(&buffer[currentChar]);
		currentChar+=4;
		lableID = charToInt32(&buffer[currentChar]);
		currentChar+=4;

		if (buffer.at(currentChar)==0)
		{
			currentChar++;
			TEXT_ALIGN_HOR align = CENTER;
			if (buffer.at(currentChar)==0)
			{
				align = LEFT;
			}
			else if (buffer.at(currentChar)==2)
			{
				align = RIGHT;
			}
			currentChar++;
			engineGUI._guiWindows.at(engineGUI.findWindowByID(windowID))->_guiGadgetLables.at(engineGUI._guiWindows.at(engineGUI.findWindowByID(windowID))->findLableById(lableID))->setHorizontalAlign(align);
		}
		else
		{
			currentChar++;
			TEXT_ALIGN_VER align = MIDDLE;
			if (buffer.at(currentChar)==0)
			{
				align = TOP;
			}
			else if (buffer.at(currentChar)==2)
			{
				align = BOTTOM;
			}
			currentChar++;
			engineGUI._guiWindows.at(engineGUI.findWindowByID(windowID))->_guiGadgetLables.at(engineGUI._guiWindows.at(engineGUI.findWindowByID(windowID))->findLableById(lableID))->setVerticalAlign(align);
=======
		if (callBackFunction=="Engine::quit")
		{
			engineGUI._guiWindows.at(engineGUI.findWindowByID(windowID))->_guiGadgetButtons.at(engineGUI._guiWindows.at(engineGUI.findWindowByID(windowID))->findButtonById(buttonID))->setCallback([&](){ gameEngine.quit(); });
>>>>>>> 59b8ca550010837924817d1561f03a061633f6b2
		}
		currentChar++;
	}
	else
	{
<<<<<<< HEAD
		//std::cout << opCode << " " << currentChar << std::endl;
=======
>>>>>>> 59b8ca550010837924817d1561f03a061633f6b2
		break;
	}
	}
	return true;
}

<<<<<<< HEAD
void Room::destroy()
{
	delete this;
}

bool Room::load()
{
	std::ifstream fileStream;
	fileStream.open(this->_file.c_str(), std::ios::binary);
	if (!fileStream.is_open())
	{
		gameEngine.error(1,"RoomManager::loadFromFile - "+this->_file);
		return -1;
	}
	if (gameRoom._currentRoom)
	{
		gameRoom.unloadRoom();
	}
	getline(fileStream, this->_name);
	std::vector<unsigned char> currentCommand;
	std::vector<unsigned char> buffer((std::istreambuf_iterator<char>(fileStream)), (std::istreambuf_iterator<char>()));
	return this->runBuffer(buffer);
}


bool Room::loadFromFile(std::string file)
{
	this->_file = file;
	return this->load();
}

void RoomManager::loadFromFile(std::string file)
{
	Room* newRoom = new Room();
	if (newRoom->loadFromFile(file))
	{
		this->_currentRoom = newRoom;
		this->_success = true;
	}
	else
	{
		newRoom->destroy();
		this->_success = false;
	}
}

void RoomManager::unloadRoom()
{
	if (this->_currentRoom!=NULL)
	{
		this->_currentRoom->destroy();
		engineGUI.destroyAllWindows();
		engineGUI.destroyAllImages();
	}
=======
int RoomManager::loadFromFile(std::string file)
{
	std::ifstream fileStream;
	fileStream.open(file.c_str(), std::ios::binary);
	if (!fileStream.is_open())
	{
		gameEngine.error(1,"RoomManager::loadFromFile - "+file);
		return -1;
	}
	Room* newRoom = new Room();
	getline(fileStream, newRoom->_name);
	std::vector<unsigned char> currentCommand;
	std::vector<unsigned char> buffer((std::istreambuf_iterator<char>(fileStream)), (std::istreambuf_iterator<char>()));
	newRoom->runBuffer(buffer);
	this->_rooms.insert(this->_rooms.end(),newRoom);
	return this->_rooms.size()-1;
>>>>>>> 59b8ca550010837924817d1561f03a061633f6b2
}

RoomManager gameRoom;

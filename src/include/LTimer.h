/*
 * LTimer.h
 *
 *  Created on: Jan 31, 2016
 *      Author: Steffen Kremer / Lazy Foo
 */

#ifndef INCLUDE_LTIMER_H_
#define INCLUDE_LTIMER_H_

class LTimer
{
    public:
		//Initializes variables
		LTimer();

		//The various clock actions
		void start();
		void stop();
		void pause();
		void unpause();

		//Gets the timer's time
		unsigned int getTicks();

		//Checks the status of the timer
		bool isStarted();
		bool isPaused();

    private:
		//The clock time when the timer started
		unsigned int mStartTicks;

		//The ticks stored when the timer was paused
		unsigned int mPausedTicks;

		//The timer status
		bool mPaused;
		bool mStarted;
};

#endif /* INCLUDE_LTIMER_H_ */

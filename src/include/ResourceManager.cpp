/*
 * ResourceManager.cpp
 *
 *  Created on: Feb 1, 2016
 *      Author: Steffen Kremer
 */

#include <iostream>
#include <vector>
#include "Engine.h"
#include "ResourceManager.h"

//#############################################################

TTF_Font *ResourceManager::addFont(std::string fontFile, int fSize)
{
	if (this->_resourceFonts.size())
	{
		for (unsigned int i=0; i < this->_resourceFonts.size(); i++)
		{
			if (this->_resourceFonts.at(i)->_fontFile==fontFile&&this->_resourceFonts.at(i)->_fontSize==fSize)
			{
				if (this->_resourceFonts.at(i)->_fontSize==fSize)
				{
					return this->_resourceFonts.at(i)->_font;
				}
				else
				{
					return 0;
				}
			}
		}
	}
	this->_resourceFonts.insert(this->_resourceFonts.end(), new ResourceFont());
	if (this->_resourceFonts.back()->loadFromFile(fontFile,fSize))
	{
		return this->_resourceFonts.back()->_font;
	}
	else
	{
		this->_resourceFonts.back()->destroy();
		this->_resourceFonts.erase(this->_resourceFonts.end());
		return NULL;
	}
}

void ResourceManager::destroy()
{
	if (this->_gameMusic._music)
	{
		this->_gameMusic.destroy();
	}
	if (this->_gameSoundGUIButtonPressed._sound)
	{
		this->_gameSoundGUIButtonPressed.destroy();
	}
	if (this->_gameSoundGameStartup._sound)
	{
		this->_gameSoundGameStartup.destroy();
	}
	if (this->_gameSoundGameStartup._sound)
	{
		this->_gameSoundGameStartup.destroy();
	}
	if (this->_gameSoundGameShutdown._sound)
	{
		this->_gameSoundGameShutdown.destroy();
	}
}

//#############################################################

bool ResourceSound::loadFromFile(std::string file)
{
	if (this->_sound)
	{
		Mix_FreeChunk(this->_sound);
	}
	this->_sound = Mix_LoadWAV(file.c_str());
	if (this->_sound==NULL)
	{
		return false;
	}
	this->_soundFile = file;
	return true;
}

void ResourceSound::play()
{
	if (!this->_sound)
	{
		return;
	}
	Mix_PlayChannel(-1, this->_sound, 0);
}

/*void ResourceSound::setVolume(int volume)
{
	if (this->_sound)
	{
		Mix_VolumeMusic(volume);
	}
}*/

void ResourceSound::destroy()
{
	if (this->_sound)
	{
		Mix_FreeChunk(this->_sound);
	}
	delete this;
}

//#############################################################

bool ResourceMusic::loadFromFile(std::string file)
{
	if (this->_music)
	{
		Mix_FreeMusic(this->_music);
	}
	this->_music = Mix_LoadMUS(file.c_str());
	if (this->_music==NULL)
	{
		return false;
	}
	this->_musicFile = file;
	return true;
}

void ResourceMusic::play()
{
	if (!this->_music)
	{
		return;
	}
	if (Mix_PlayingMusic()==0)
	{
		Mix_PlayMusic(this->_music, -1);
	}
	else if (Mix_PausedMusic()==1)
	{
		Mix_ResumeMusic();
	}
}

void ResourceMusic::setVolume(int volume)
{
	if (this->_music)
	{
		Mix_VolumeMusic(volume);
	}
}

void ResourceMusic::destroy()
{
	if (this->_music)
	{
		Mix_FreeMusic(this->_music);
	}
	delete this;
}

//#############################################################

bool ResourceSound::loadFromFile(std::string file)
{
	if (this->_sound)
	{
		Mix_FreeChunk(this->_sound);
	}
	this->_sound = Mix_LoadWAV(file.c_str());
	if (this->_sound==NULL)
	{
		return false;
	}
	this->_soundFile = file;
	return true;
}

void ResourceSound::play()
{
	if (!this->_sound)
	{
		return;
	}
	Mix_PlayChannel(-1, this->_sound, 0);
}

/*void ResourceSound::setVolume(int volume)
{
	if (this->_sound)
	{
		Mix_VolumeMusic(volume);
	}
}*/

void ResourceSound::destroy()
{
	if (this->_sound)
	{
		Mix_FreeChunk(this->_sound);
	}
	delete this;
}

//#############################################################

bool ResourceMusic::loadFromFile(std::string file)
{
	if (this->_music)
	{
		Mix_FreeMusic(this->_music);
	}
	this->_music = Mix_LoadMUS(file.c_str());
	if (this->_music==NULL)
	{
		return false;
	}
	this->_musicFile = file;
	return true;
}

void ResourceMusic::play()
{
	if (!this->_music)
	{
		return;
	}
	if (Mix_PlayingMusic()==0)
	{
		Mix_PlayMusic(this->_music, -1);
	}
	else if (Mix_PausedMusic()==1)
	{
		Mix_ResumeMusic();
	}
}

void ResourceMusic::setVolume(int volume)
{
	if (this->_music)
	{
		Mix_VolumeMusic(volume);
	}
}

void ResourceMusic::destroy()
{
	if (this->_music)
	{
		Mix_FreeMusic(this->_music);
	}
	delete this;
}

//#############################################################

bool ResourceFont::loadFromFile(std::string file, int fSize)
{
	this->_font = TTF_OpenFont(file.c_str(), fSize);
	this->_fontSize = fSize;
	this->_fontFile = file;
	if (!this->_font)
	{
		gameEngine._sdlError = SDL_GetError();
		std::cout << gameEngine._sdlError;
		gameEngine.error(8,"TTF_OpenFont");
		return false;
	}
	return true;
}

void ResourceFont::destroy()
{
	if (this->_font)
	{
		TTF_CloseFont(this->_font);
	}
	delete this;
}

//#############################################################

bool ResourceTexture::createFromFile(std::string file, int x, int y, float sx, float sy)
{
	this->_data = file;
	this->_position.x = x;
	this->_position.y = y;
	SDL_Surface *tmp = SDL_LoadBMP(this->_data.c_str());
	if (tmp==NULL)
	{
		gameEngine._sdlError = SDL_GetError();
		gameEngine.error(7,"SDL_LoadBMP: " + this->_data);
		return false;
	}
	SDL_Surface *tmp2 = SDL_ConvertSurfaceFormat(tmp,gameEngine._sdlPixelFormat,0);
	SDL_FreeSurface(tmp);
	if (tmp2==NULL)
	{
		gameEngine._sdlError = SDL_GetError();
		gameEngine.error(8,"SDL_ConvertSurface: " + file);
		return false;
	}
	this->_size.w = tmp2->w;
	this->_size.h = tmp2->h;
	this->_position.w = this->_size.w * sx;
	this->_position.h = this->_size.h * sy;
	SDL_Rect src;
	src.x = 0;
	src.y = 0;
	src.w = this->_size.w * sx;
	src.h = this->_size.h * sy;
	SDL_BlitSurface(tmp2, &src, gameEngine._sdlSurface, &this->_size);
	if (this->_texture!=NULL)
	{
		SDL_DestroyTexture(this->_texture);
	}
	this->_texture = SDL_CreateTextureFromSurface(gameEngine._sdlRenderer, tmp2);
	if (this->_texture==NULL)
	{
		gameEngine._sdlError = SDL_GetError();
		gameEngine.error(7,"SDL_CreateTextureFromSurface: " + this->_data);
		return false;
	}
	SDL_FreeSurface(tmp2);
	this->_isText = false;
	return true;
}

bool ResourceTexture::createFromText(std::string text, int fSize, int x, int y, SDL_Color foreGround)
{
	//fSize = fSize*gameEngine._displayDensityX;
	if (fSize<3)
	{
		fSize = 3;
	}
	TTF_Font *tmp = NULL;
	//std::cout << engineResource.addFont(gameEngine._sdlTTFfontFile,fSize);
	//TTF_Font *tmp = NULL;
	//return false;
	/*if (!tmp)
	{
		gameEngine.error(8,"TTF_OpenFont");
		//return false;
	}*/
	this->_data = text;
	this->_position.x = x;
	this->_position.y = y;
	this->_position.w = 0;
	this->_position.h = 0;
	this->_size.x = x;
	this->_size.y = y;
	this->_size.w = 0;
	this->_size.h = 0;
	SDL_Surface *tmpS = TTF_RenderText_Solid(engineResource.addFont(gameEngine._sdlTTFfontFile,fSize),this->_data.c_str(),foreGround);
	if (tmpS==NULL)
	{
		TTF_CloseFont(tmp);
		gameEngine._sdlError = TTF_GetError();
		gameEngine.error(9,"TTF_RenderText");
		return false;
	}
	TTF_SizeText(engineResource.addFont(gameEngine._sdlTTFfontFile,fSize), this->_data.c_str(), &this->_size.w, &this->_size.h);
	this->_position.w = this->_size.w;
	this->_position.h = this->_size.h;
	if (this->_texture!=NULL)
	{
		SDL_DestroyTexture(this->_texture);
	}
	this->_texture = SDL_CreateTextureFromSurface(gameEngine._sdlRenderer, tmpS);
	if (this->_texture==NULL)
	{
		TTF_CloseFont(tmp);
		gameEngine._sdlError = SDL_GetError();
		gameEngine.error(7,"SDL_CreateTextureFromSurface: " + this->_data);
		return false;
	}
	SDL_FreeSurface(tmpS);
	TTF_CloseFont(tmp);
	this->_isText = true;
	this->_fSize = fSize;
	this->_textColor = foreGround;
	return true;
}

bool ResourceTexture::changeText(std::string text)
{
	return this->createFromText(text, this->_fSize, this->_size.x, this->_size.y, this->_textColor);
}

void ResourceTexture::resize(float sx, float sy)
{
	this->_position.w = this->_size.w * sx;
	this->_position.h = this->_size.h * sy;
}

void ResourceTexture::draw()
{
	if (!this->_texture)
	{
		return;
	}
	if (this->_visible)
	{
		if (gameEngine._debugDraw)
		{
			SDL_SetRenderDrawColor(gameEngine._sdlRenderer,0,255,255,2);
			SDL_RenderDrawRect(gameEngine._sdlRenderer,&this->_position);
		}
		if (this->_texture)
		{
			SDL_RenderCopy(gameEngine._sdlRenderer, this->_texture, NULL, &this->_position );
		}
	}
}

void ResourceTexture::update(int x, int y)
{
	this->_position.x = x;
	this->_position.y = y;
}

void ResourceTexture::destroy()
{
	if (this->_texture)
	{
		SDL_DestroyTexture(this->_texture);
	}
	delete this;
}

//#############################################################

ResourceManager engineResource;

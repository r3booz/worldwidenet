/*
 * GUI.cpp
 *
 *  Created on: Jan 19, 2016
 *      Author: Steffen Kremer
 */

#include <functional>
#include <iostream>
#include <algorithm>
#include "GUI.h"
#include "Engine.h"
#include "SDL.h"
#include "SDL_ttf.h"
#undef main
#include "EngineMisc.h"

GUIWindow* GUI::createWindow(std::string title, int x, int y, int sx, int sy)
{
	this->_guiWindows.insert(this->_guiWindows.end(), new GUIWindow(title,x,y,sx,sy));
	return this->_guiWindows.at(this->_guiWindows.size()-1);
}

GUIImage* GUI::createImage(std::string file, int x, int y, float sx, float sy)
{
	this->_guiImage.insert(this->_guiImage.end(), new GUIImage(NULL, x, y, sx, sy, file));
	return this->_guiImage.at(this->_guiImage.size()-1);
}

void GUI::showMouseCursor(bool show)
{
	this->_showMouseCursor = show;
}

void GUI::draw()
{
	for (unsigned int i=0; i < this->_guiImage.size(); i++)
	{
		this->_guiImage.at(i)->draw();
	}
	for (unsigned int i=0; i < this->_guiWindows.size(); i++)
	{
		this->_guiWindows.at(i)->draw();
	}
	if (this->_showMouseCursor)
	{
		SDL_SetRenderDrawColor(gameEngine._sdlRenderer,255,100,0,2);
		SDL_Rect top;
		top.x = this->_mouseStateX-3;
		top.y = this->_mouseStateY-3;
		top.w = 6;
		top.h = 6;
		SDL_RenderDrawRect(gameEngine._sdlRenderer,&top);
		SDL_SetRenderDrawColor(gameEngine._sdlRenderer,255,0,0,2);
		SDL_RenderDrawLine(gameEngine._sdlRenderer,top.x,top.y,top.x+5,top.y+5);
		SDL_RenderDrawLine(gameEngine._sdlRenderer,top.x+5,top.y,top.x,top.y+5);
	}
}

void GUI::update()
{
	for (unsigned int i=engineGUI._guiWindows.size()-1; i >= 0; i--)
	{
		if (i==4294967295)
		{
			break;
		}
		this->_guiWindows.at(i)->updateBackward();
	}
	if (this->_guiWindows.size()>1)
	{
		for (unsigned int i=0; i < this->_guiWindows.size(); i++)
		{
			if (this->_guiWindows.at(i)->_focused)
			{
				this->_guiWindows.back()->_focused = false;
				std::rotate(this->_guiWindows.begin() + i, this->_guiWindows.begin() + i + 1, this->_guiWindows.end());
			}
		}
		this->_guiWindows.back()->_focused = true;
	}
	else if (this->_guiWindows.size()>0)
	{
		this->_guiWindows.at(0)->_focused = true;
	}
	for (unsigned int i=0; i < this->_guiImage.size(); i++)
	{
		this->_guiImage.at(i)->update();
	}
	for (unsigned int i=0; i < engineGUI._guiWindows.size(); i++)
	{
		this->_guiWindows.at(i)->update();
	}
	engineGUI._windowAlreadyCollided = false;
}

unsigned int GUI::findWindowByPointer(GUIWindow* pointer)
{
	for (unsigned int i=0; i < engineGUI._guiWindows.size(); i++)
	{
		if (this->_guiWindows.at(i)==pointer)
		{
			return i;
		}
	}
	return 4294967295;
}

unsigned int GUI::findWindowByID(unsigned int windowID)
{
	for (unsigned int i=0; i < engineGUI._guiWindows.size(); i++)
	{
		if (this->_guiWindows.at(i)->_windowID==windowID)
		{
			return i;
		}
	}
	return 4294967295;
}

void GUI::destroyWindow(unsigned int index)
{
	if (index<=this->_guiWindows.size()-1&&index>=0)
	{
		this->_guiWindows.at(index)->destroy();
	}
}

void GUI::destroyAllWindows()
{
	if (this->_guiWindows.size())
	{
		unsigned int s = this->_guiWindows.size()-1;
		while(1)
		{
			this->_guiWindows.at(s)->destroy();
			if (s==0)
			{
				break;
			}
			s--;
		}
		this->_guiWindows.clear();
	}
}

void GUI::destroyAllImages()
{
	if (this->_guiImage.size())
	{
		unsigned int s = this->_guiImage.size()-1;
		while(1)
		{
			this->_guiImage.at(s)->destroy();
			if (s==0)
			{
				break;
			}
			s--;
		}
		this->_guiImage.clear();
	}
}

//########################################

GUIWindow::GUIWindow(std::string title, int x, int y, int sx, int sy)
{
	this->_title = title;
	this->_position.x = x;
	this->_position.y = y+30;
	this->_position.w = sx;
	this->_position.h = sy;
	this->_maximizeAble=(!this->_popUp&&this->_maximizeAble);
	this->_minimizeAble=(!this->_popUp&&this->_minimizeAble);
	this->_titleGUItext = new GUIText(title, x+6, y-25+30, 20, 0, 255, 0);
	this->_backColor = {0,30,0,255};
	this->update();
}

bool GUIWindow::changeTitle(std::string text)
{
	return this->_titleGUItext->changeText(text);
}

void GUIWindow::setBackColor(Uint8 r, Uint8 g, Uint8 b)
{
	this->_backColor = {r,g,b,255};
}

void GUIWindow::setProperties(bool moveAble, bool resizeAble, bool closeAble, bool minimizeAble, bool maximizeAble, bool borderLess, bool popUp)
{
	this->_moveAble = moveAble;
	this->_resizeAble = resizeAble;
	this->_closeAble = closeAble;
	this->_minimizeAble = minimizeAble;
	this->_maximizeAble = maximizeAble;
	this->_borderLess = borderLess;
	this->_popUp = popUp;
}

GUIGadgetLable* GUIWindow::createGadgetLable(std::string lable, int x, int y, int sx, int sy)
{
	this->_guiGadgetLables.insert(this->_guiGadgetLables.end(), new GUIGadgetLable(this, lable, x, y, sx, sy));
	return this->_guiGadgetLables.at(this->_guiGadgetLables.size()-1);
}

GUIGadgetButton* GUIWindow::createGadgetButton(std::string lable, int x, int y, int sx, int sy)
{
	this->_guiGadgetButtons.insert(this->_guiGadgetButtons.end(), new GUIGadgetButton(this, lable, x, y, sx, sy));
	return this->_guiGadgetButtons.at(this->_guiGadgetButtons.size()-1);
}

GUIImage* GUIWindow::createGadgetImage(std::string file, int x, int y, float sx, float sy)
{
	this->_guiGadgetImages.insert(this->_guiGadgetImages.end(), new GUIImage(this, x, y, sx, sy, file));
	return this->_guiGadgetImages.at(this->_guiGadgetImages.size()-1);
}

void GUIWindow::pushEvent(EVENT_TYPE event)
{
	this->_windowEvents.insert(this->_windowEvents.end(), new Event());
	this->_windowEvents.at(this->_windowEvents.size()-1)->_eventType = event;
}
EVENT_TYPE GUIWindow::popEvent()
{
	EVENT_TYPE t = EVENT_NONE;
	if (this->_windowEvents.size()>0)
	{
		t = this->_windowEvents.at(this->_windowEvents.size()-1)->_eventType;
		this->_windowEvents.pop_back();//erase(this->_windowEvents.end());
	}
	return t;
}

unsigned int GUIWindow::findButtonById(unsigned int id)
{
	for (unsigned int i=0; i < this->_guiGadgetButtons.size(); i++)
	{
		if (this->_guiGadgetButtons.at(i)->_buttonID==id)
		{
			return i;
		}
	}
	return 4294967295;
}

<<<<<<< HEAD
unsigned int GUIWindow::findLableById(unsigned int id)
{
	for (unsigned int i=0; i < this->_guiGadgetLables.size(); i++)
	{
		if (this->_guiGadgetLables.at(i)->_lableID==id)
		{
			return i;
		}
	}
	return 4294967295;
}

=======
>>>>>>> 59b8ca550010837924817d1561f03a061633f6b2
void GUIWindow::draw()
{
	if (this->_visible)
	{
		SDL_SetRenderDrawColor(gameEngine._sdlRenderer,this->_backColor.r,this->_backColor.g,this->_backColor.b,this->_backColor.a);
		SDL_Rect top;
		top.x = this->_position.x;
		top.y = this->_position.y;
		top.w = this->_position.w;
		top.h = this->_position.h;
		SDL_RenderFillRect(gameEngine._sdlRenderer,&top);
		if (!this->_borderLess)
		{
			if (this->_focused)
			{
				SDL_SetRenderDrawColor(gameEngine._sdlRenderer,0,70,0,255);
			}
			else
			{
				SDL_SetRenderDrawColor(gameEngine._sdlRenderer,0,40,0,255);
			}
			top.x = this->_position.x;
			top.y = this->_position.y-30;
			top.w = this->_position.w;
			top.h = 30;
			SDL_RenderFillRect(gameEngine._sdlRenderer,&top);
		}
		if (gameEngine._debugDraw)
		{
			SDL_SetRenderDrawColor(gameEngine._sdlRenderer,255,0,0,2);
		}
		else
		{
			SDL_SetRenderDrawColor(gameEngine._sdlRenderer,0,255,0,2);
		}
		SDL_RenderDrawRect(gameEngine._sdlRenderer,&this->_position);
		if (!this->_borderLess)
		{
			top.x = this->_position.x;
			top.y = this->_position.y-30;
			top.w = this->_position.w;
			top.h = 30;
			SDL_RenderDrawRect(gameEngine._sdlRenderer,&top);
		}
		if (this->_closeAble)
		{
			top.x = this->_position.x+this->_position.w-30;
			top.y = this->_position.y-25;
			top.w = 20;
			top.h = 20;
			SDL_RenderDrawRect(gameEngine._sdlRenderer,&top);
			SDL_RenderDrawLine(gameEngine._sdlRenderer,top.x,top.y+19,top.x+19,top.y);
			SDL_RenderDrawLine(gameEngine._sdlRenderer,top.x,top.y,top.x+19,top.y+19);

		}
		if (this->_maximizeAble)
		{
			top.x = this->_position.x+this->_position.w-55;
			top.y = this->_position.y-25;
			top.w = 20;
			top.h = 20;
			SDL_RenderDrawRect(gameEngine._sdlRenderer,&top);
			if (this->_maximized)
			{
				top.x = this->_position.x+this->_position.w-55+5;
				top.y = this->_position.y-23;
				top.w = 13;
				top.h = 13;
				SDL_RenderDrawRect(gameEngine._sdlRenderer,&top);
			}
		}
		if (this->_minimizeAble)
		{
			top.x = this->_position.x+this->_position.w-80;
			top.y = this->_position.y-25;
			top.w = 20;
			top.h = 20;
			SDL_RenderDrawRect(gameEngine._sdlRenderer,&top);
			top.x = this->_position.x+this->_position.w-80+2;
			top.y = this->_position.y-13;
			top.w = 16;
			top.h = 5;
			SDL_RenderDrawRect(gameEngine._sdlRenderer,&top);
		}
		for (unsigned int i=0; i < this->_guiGadgetImages.size(); i++)
		{
			this->_guiGadgetImages.at(i)->draw();
		}
		for (unsigned int i=0; i < this->_guiGadgetLables.size(); i++)
		{
			this->_guiGadgetLables.at(i)->draw();
		}
		for (unsigned int i=0; i < this->_guiGadgetButtons.size(); i++)
		{
			this->_guiGadgetButtons.at(i)->draw();
		}
		if (!this->_borderLess&&this->_titleGUItext)
		{
			this->_titleGUItext->draw();
		}
	}
}

void GUIWindow::updateBackward()
{
	if (!engineGUI._windowAlreadyCollided)
	{
		if (!engineGUI._mouseStateLeftStateLast&&engineGUI._mouseStateLeftState&&posIsInRect(engineGUI._mouseStateX, engineGUI._mouseStateY, this->_position.x, this->_position.y-30, this->_position.w,  this->_position.h+30))
		{
			engineGUI._windowAlreadyCollided = true;
			this->_focused = true;
		}
	}
}

void GUIWindow::update()
{
	if (this->_closed)
	{
		this->_visible = false;
		if (engineGUI._guiWindows.size()>1&&!this->_closedAlready) //need second trigger to avoid refocus every time (blinking)
		{
			this->_closedAlready = true;
			engineGUI._guiWindows.at(engineGUI._guiWindows.size()-2)->_focused = true;
		}
		return;
	}
	if (this->_borderLess)
	{
		this->_maximizeAble = false;
		this->_minimizeAble = false;
		this->_moveAble = false;
		this->_closeAble = false;
	}
	if (!this->_borderLess&&this->_focused)
	{
		if (!engineGUI._mouseStateLeftStateLast&&engineGUI._mouseStateLeftState&&posIsInRect(engineGUI._mouseStateX, engineGUI._mouseStateY, this->_position.x, this->_position.y-30, this->_position.w, 30))
		{
			if (posIsInRect(engineGUI._mouseStateX, engineGUI._mouseStateY, this->_position.x+this->_position.w-30, this->_position.y-25, 20, 20)&&this->_closeAble)
			{
				this->_closed = true;
				this->destroy();
			}
			else if (posIsInRect(engineGUI._mouseStateX, engineGUI._mouseStateY, this->_position.x+this->_position.w-55, this->_position.y-25, 20, 20)&&this->_maximizeAble)
			{
				if (this->_maximized)
				{
					this->_maximized = false;
				}
				else
				{
					this->_maximized = true;
					this->_maximizedLast = false;
				}
			}
			else if (posIsInRect(engineGUI._mouseStateX, engineGUI._mouseStateY, this->_position.x+this->_position.w-80, this->_position.y-25, 20, 20)&&this->_maximizeAble)
			{
				this->_minimized = true;
				//this->_focused = true;
			}
			else
			{
				this->_grabbed = true;
			}
		}
	}
	if (this->_maximized||!this->_moveAble)
	{
		this->_grabbed = false;
	}
	if (!engineGUI._mouseStateLeftState)
	{
		this->_grabbed = false;
	}
	if (this->_grabbed)
	{
		this->_position.x = this->_position.x+engineGUI._mouseStateXmove;
		this->_position.y = this->_position.y+engineGUI._mouseStateYmove;
	}
	this->_titleGUItext->update(this->_position.x+6,this->_position.y-25);
	this->updateGadgets();
	if (this->_maximized&&!this->_maximizedLast)
	{
		this->_maximizedOldPosition.x = this->_position.x;
		this->_maximizedOldPosition.y = this->_position.y;
		this->_maximizedOldPosition.w = this->_position.w;
		this->_maximizedOldPosition.h = this->_position.h;
		this->_position.x = 0;
		this->_position.y = 30;
		this->_position.w = gameEngine._displaySizeX;
		this->_position.h = gameEngine._displaySizeY-30;
		this->_maximizedLast = true;
		this->update();
	}
	else if (!this->_maximized&&this->_maximizedLast)
	{
		this->_maximizedLast = false;
		this->_position.x = this->_maximizedOldPosition.x;
		this->_position.y = this->_maximizedOldPosition.y;
		this->_position.w = this->_maximizedOldPosition.w;
		this->_position.h = this->_maximizedOldPosition.h;
		this->update();
	}
}

void GUIWindow::updateGadgets()
{
	for (unsigned int i=0; i < this->_guiGadgetImages.size(); i++)
	{
		this->_guiGadgetImages.at(i)->update();
	}
	for (unsigned int i=0; i < this->_guiGadgetLables.size(); i++)
	{
		this->_guiGadgetLables.at(i)->update();
	}
	for (unsigned int i=0; i < this->_guiGadgetButtons.size(); i++)
	{
		this->_guiGadgetButtons.at(i)->update();
	}
}

void GUIWindow::destroy()
{
	for (unsigned int i=0; i < this->_guiGadgetImages.size(); i++)
	{
		this->_guiGadgetImages.at(i)->destroy();
	}
	this->_guiGadgetImages.clear();
	for (unsigned int i=0; i < this->_guiGadgetLables.size(); i++)
	{
		this->_guiGadgetLables.at(i)->destroy();
	}
	this->_guiGadgetLables.clear();
	return;
	for (unsigned int i=0; i < this->_guiGadgetButtons.size(); i++)
	{
		this->_guiGadgetButtons.at(i)->destroy();
	}
	this->_guiGadgetButtons.clear();
<<<<<<< HEAD
	if (this->_titleGUItext)
	{
		this->_titleGUItext->destroy();
	}
	return;
=======
>>>>>>> 59b8ca550010837924817d1561f03a061633f6b2
	engineGUI._guiWindows.erase(engineGUI._guiWindows.begin() + engineGUI.findWindowByPointer(this));
	delete this;
}

//########################################

GUIImage::GUIImage(GUIWindow *pWindow, int x, int y, float sx, float sy, std::string file)
{
	this->_parentWindow = pWindow;
	if (this->_parentWindow)
	{
		this->_resourceTexture = new ResourceTexture();
		if (this->_resourceTexture->createFromFile(file,this->_parentWindow->_position.x,this->_parentWindow->_position.x,sx,sy))
		{
			this->_resourceTexture->_size.x = x;
			this->_resourceTexture->_size.y = y;
		}
	}
	else
	{
		this->_resourceTexture = new ResourceTexture();
		if (this->_resourceTexture->createFromFile(file,x,y,sx,sy))
		{
			this->_resourceTexture->_size.x = x;
			this->_resourceTexture->_size.y = y;
		}
	}
}

void GUIImage::draw()
{
	if (this->_resourceTexture)
	{
		this->_resourceTexture->draw();
	}
}

void GUIImage::update()
{
	if (this->_parentWindow)
	{
		this->_resourceTexture->update(this->_resourceTexture->_size.x + this->_parentWindow->_position.x, this->_resourceTexture->_size.x + this->_parentWindow->_position.y);
	}
}

void GUIImage::destroy()
{
	if (this->_resourceTexture)
	{
		this->_resourceTexture->destroy();
	}
	delete this;
}

void GUIImage::setVisible(bool visible)
{
	this->_resourceTexture->_visible = visible;
}

//########################################

GUIText::GUIText(std::string text, int x, int y, int fSize, Uint8 r, Uint8 g, Uint8 b)
{
	this->_resourceTexture = new ResourceTexture();
	SDL_Color tmp = {r,g,b};
	this->_resourceTexture->createFromText(text,fSize,x,y,tmp);
}

void GUIText::setVisible(bool visible)
{
	if (this->_resourceTexture)
	{
		this->_resourceTexture->_visible = true;
	}
}

bool GUIText::changeText(std::string text)
{
	if (this->_resourceTexture)
	{
		return this->_resourceTexture->changeText(text);
	}
	else
	{
		return false;
	}
}

void GUIText::update(int x, int y)
{
	if (this->_resourceTexture)
	{
		this->_resourceTexture->update(x,y);
	}
}

void GUIText::draw()
{
	if (this->_resourceTexture)
	{
		this->_resourceTexture->draw();
	}
}

void GUIText::destroy()
{
	if (this->_resourceTexture)
	{
		this->_resourceTexture->destroy();
	}
	delete this;
}

//########################################

GUIGadgetLable::GUIGadgetLable(GUIWindow *pWindow, std::string lable, int x, int y, int sx, int sy)
{
	this->_parentWindow = pWindow;
	this->_size.x=x;
	this->_size.y=y;
	this->_size.w=sx;
	this->_size.h=sy;
	this->_guiText = new GUIText(lable, x, y, 18, 0, 255, 0);
	this->update();
}

bool GUIGadgetLable::changeText(std::string text)
{
	if (this->_guiText)
	{
		return this->_guiText->changeText(text);
	}
	else
	{
		return false;
	}
}

void GUIGadgetLable::setVisible(bool visible)
{
	if (this->_guiText)
	{
		this->_guiText->setVisible(visible);
	}
}

void GUIGadgetLable::setHorizontalAlign(TEXT_ALIGN_HOR newAlign)
{
	this->_horizontalAlign = newAlign;
}

void GUIGadgetLable::setVerticalAlign(TEXT_ALIGN_VER newAlign)
{
	this->_verticalAlign = newAlign;
}

void GUIGadgetLable::update()
{
	this->_rect.x=this->_size.x+this->_parentWindow->_position.x;
	this->_rect.y=this->_size.y+this->_parentWindow->_position.y;
	this->_rect.w=this->_size.w;
	this->_rect.h=this->_size.h;
	if (this->_horizontalAlign==CENTER)
	{
		this->_rect.x += (this->_size.w/2)-(this->_guiText->_resourceTexture->_size.w/2);
	}
	else if (this->_horizontalAlign==RIGHT)
	{
		this->_rect.x += this->_size.w-this->_guiText->_resourceTexture->_size.w-3;
	}
	else
	{
		this->_rect.x += 3;
	}
	if (this->_verticalAlign==MIDDLE)
	{
		this->_rect.y += (this->_size.h/2)-(this->_guiText->_resourceTexture->_size.h/2);
	}
	else if (this->_verticalAlign==BOTTOM)
	{
		this->_rect.y += this->_size.h-this->_guiText->_resourceTexture->_size.h-3;
	}
	else
	{
		this->_rect.y += 3;
	}
	if (this->_guiText)
	{
		this->_guiText->_resourceTexture->update(this->_rect.x, this->_rect.y);
	}
}

void GUIGadgetLable::draw()
{
	if (gameEngine._debugDraw)
	{
		SDL_SetRenderDrawColor(gameEngine._sdlRenderer,255,255,0,2);
		SDL_RenderDrawRect(gameEngine._sdlRenderer,&this->_rect);
	}
	if (this->_guiText)
	{
		this->_guiText->draw();
	}
}

void GUIGadgetLable::destroy()
{
	if (this->_guiText)
	{
		this->_guiText->destroy();
	}
	delete this;
}

//########################################

GUIGadgetButton::GUIGadgetButton(GUIWindow *pWindow, std::string lable, int x, int y, int sx, int sy)
{
	this->_lableText = new GUIGadgetLable(pWindow,lable,x,y,sx,sy);
	this->_lableText->_horizontalAlign = CENTER;
	this->_parentWindow = pWindow;
	this->_lable = lable;
	this->_size.x=x;
	this->_size.y=y;
	this->_size.w=sx;
	this->_size.h=sy;
	this->update();
}

bool GUIGadgetButton::changeText(std::string text)
{
	return this->_lableText->changeText(text);
}

<<<<<<< HEAD
/*void GUIGadgetButton::setCallbackVoid(std::function<void(void)> callback)
{
	this->_functionCallbackVoid = callback;
}

void GUIGadgetButton::setCallbackString(std::function<void(std::string)> callback)
{
	this->_functionCallbackString = callback;
}*/

=======
void GUIGadgetButton::setCallback(std::function<void(void)> callback)
{
	this->_functionCallback = callback;
}

>>>>>>> 59b8ca550010837924817d1561f03a061633f6b2
void GUIGadgetButton::update()
{
	this->_rect.x=this->_size.x+this->_parentWindow->_position.x;
	this->_rect.y=this->_size.y+this->_parentWindow->_position.y;
	this->_rect.w=this->_size.w;
	this->_rect.h=this->_size.h;
	if (this->_parentWindow->_focused&&posIsInRect(engineGUI._mouseStateX,engineGUI._mouseStateY,this->_rect))
	{
		if (engineGUI._mouseStateLeftState)
		{
			this->_pressed = true;
		}
		else if (this->_pressed&&engineGUI._mouseStateLeftStateLast&&!engineGUI._mouseStateLeftState)
		{
			this->_pressed = false;
			engineResource._gameSoundGUIButtonPressed.play();
<<<<<<< HEAD
			if (this->_callBack=="")//this->_functionCallbackVoid==NULL&&this->_functionCallbackString==NULL)
=======
			if (this->_functionCallback==NULL)
>>>>>>> 59b8ca550010837924817d1561f03a061633f6b2
			{
				this->_parentWindow->pushEvent(EVENT_WINDOW_GADGET_BUTTON_PRESSED);
			}
			else
			{
<<<<<<< HEAD
				if (this->_callBack=="Engine::quit")
				{
					gameEngine.quit();
				}
				else if (this->_callBack=="RoomManager::loadRoom")
				{
					gameRoom.loadFromFile(this->_callBackParameter);
				}
				/*if (this->_parameterCallback!="")
				{
					if (this->_functionCallbackString)
					{
						this->_functionCallbackString(this->_parameterCallback);
					}
				}
				else
				{
					this->_functionCallbackVoid();
				}*/
=======
				_functionCallback();
>>>>>>> 59b8ca550010837924817d1561f03a061633f6b2
			}
			this->_parentWindow->_guiGadgetButtonLastPressed = this;
		}
		else
		{
			this->_hover = true;
			this->_pressed = false;
		}
	}
	else
	{
		this->_hover = false;
		this->_pressed = false;
	}
	this->_lableText->update();
}

void GUIGadgetButton::draw()
{
	/*if (gameEngine._debugDraw)
	{
		SDL_SetRenderDrawColor(gameEngine._sdlRenderer,255,255,0,2);
		SDL_RenderDrawRect(gameEngine._sdlRenderer,&this->_rect);
	}*/
	if (this->_visible)
	{
		if (this->_hover)
		{
			SDL_SetRenderDrawColor(gameEngine._sdlRenderer,0,60,0,255);
		}
		else
		{
			SDL_SetRenderDrawColor(gameEngine._sdlRenderer,0,50,0,255);
		}
		if (this->_pressed)
		{
			SDL_SetRenderDrawColor(gameEngine._sdlRenderer,0,70,0,255);
		}
		SDL_RenderFillRect(gameEngine._sdlRenderer,&this->_rect);
		SDL_SetRenderDrawColor(gameEngine._sdlRenderer,0,255,0,255);
		SDL_RenderDrawRect(gameEngine._sdlRenderer,&this->_rect);
		this->_lableText->draw();
	}
}

void GUIGadgetButton::destroy()
{
	this->_lableText->destroy();
	delete this;
}

//########################################

GUI engineGUI;
